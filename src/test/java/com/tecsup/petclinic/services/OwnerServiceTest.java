package com.tecsup.petclinic.services;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import com.tecsup.petclinic.entities.Owner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Test;
import com.tecsup.petclinic.exception.OwnerNotFoundException;

import java.util.List;

@SpringBootTest
public class OwnerServiceTest {

	private static final Logger logger = LoggerFactory.getLogger(OwnerServiceTest.class);

	@Autowired
   	private OwnerService ownerService;


	@Test
	public void testFindOwnerById() {

		long ID = 1;
		String NAME = "George";
		Owner owner = null;
		
		try {
			
			owner = ownerService.findById(ID);
			
		} catch (OwnerNotFoundException e) {
			assertThat(e.getMessage(), false);
		}
		logger.info("" + owner);

		assertThat(NAME, is(owner.getFirstName()));

	}

	@Test
	public void testFindOwnerByFirstName(){
		String FIND_FIRSTNAME = "George";
		int SIZE_EXPECTED =1;
		List<Owner> owners = ownerService.findByName(FIND_FIRSTNAME);
		assertThat(SIZE_EXPECTED, is(owners.size()));
	}

	@Test
	public void testFindOwnerByLastName(){
		String FIND_LASTNAME = "Franklin";
		int SIZE_EXPECTED = 1;

		List<Owner> owners = ownerService.findByLastName(FIND_LASTNAME);
		assertThat(SIZE_EXPECTED, is(owners.size()));
	}
	@Test
	public void testFindOwnerByCity(){
		String FIND_CITY = "Madison";
		int SIZE_EXPECTED = 4;

		List<Owner> owners = ownerService.findByCity(FIND_CITY);
		assertThat(SIZE_EXPECTED, is(owners.size()));
	}
	@Test
	public void testCreateOwner(){
		String First_NAME = "Carlos";
		String LAST_NAME = "Gallardo";
		String ADDRESS = "Home";
		String CITY = "Santa Anita";
		String TELEPHONE = "987654321";

		String UP_First_NAME = "Juan Smith";
		String UP_LAST_NAME = "Quispe Oxfort";
		String UP_ADDRESS = "MIAMI";
		String UP_CITY = "Miami 2PAC";
		String UP_TELEPHONE = "123456789";

		Owner owner = new Owner(First_NAME,LAST_NAME,ADDRESS,CITY,TELEPHONE);

		owner = ownerService.create(owner);
		logger.info("" + owner);

		assertThat(owner.getId(), notNullValue());
		assertThat(First_NAME, is(owner.getFirstName()));
		assertThat(LAST_NAME, is(owner.getLastName()));
		assertThat(ADDRESS, is(owner.getAddress()));
		assertThat(CITY, is(owner.getCity()));
		assertThat(TELEPHONE, is(owner.getTelephone()));
	}
	@Test
	public void testUpdateOwner(){
		String First_NAME = "Carlos";
		String LAST_NAME = "Gallardo";
		String ADDRESS = "Home";
		String CITY = "Santa Anita";
		String TELEPHONE = "987654321";
		long create_id = -1;

		String UP_First_NAME = "Juan Smith";
		String UP_LAST_NAME = "Quispe Oxfort";
		String UP_ADDRESS = "MIAMI";
		String UP_CITY = "Miami 2PAC";
		String UP_TELEPHONE = "123456789";

		Owner owner = new Owner(First_NAME,LAST_NAME,ADDRESS,CITY,TELEPHONE);

		//create record
		logger.info(">" + owner);
		Owner readOwner = ownerService.create(owner);
		logger.info(">>" + readOwner);

		create_id = readOwner.getId();

		//Prepare data for update
		readOwner.setFirstName(UP_First_NAME);
		readOwner.setLastName(UP_LAST_NAME);
		readOwner.setAddress(UP_ADDRESS);
		readOwner.setCity(UP_CITY);
		readOwner.setTelephone(UP_TELEPHONE);

		//Excute update
		Owner upgradeOwner = ownerService.update(readOwner);
		logger.info(">>>>" +upgradeOwner);

		assertThat(create_id, notNullValue());
		assertThat(create_id, is(upgradeOwner.getId()));
		assertThat(UP_First_NAME, is(upgradeOwner.getFirstName()));
		assertThat(UP_LAST_NAME, is(upgradeOwner.getLastName()));
		assertThat(UP_ADDRESS, is(upgradeOwner.getAddress()));
		assertThat(UP_CITY, is(upgradeOwner.getCity()));
		assertThat(UP_TELEPHONE, is(upgradeOwner.getTelephone()));

	}
	@Test
	public void testDeleteOwner(){
		String First_NAME = "Carlos";
		String LAST_NAME = "Gallardo";
		String ADDRESS = "Home";
		String CITY = "Santa Anita";
		String TELEPHONE = "987654321";

		Owner owner = new Owner(First_NAME,LAST_NAME,ADDRESS,CITY,TELEPHONE);
		owner = ownerService.create(owner);
		logger.info("" + owner);

		try {
			ownerService.delete(owner.getId());
		}catch (OwnerNotFoundException e){
			assertThat(e.getMessage(), false);
		}

		try{
			ownerService.findById(owner.getId());
			assertThat(true, is(false));
		}catch (OwnerNotFoundException e){
			assertThat(true, is(true));
		}
	}
}
