package com.tecsup.petclinic.services;

import com.tecsup.petclinic.entities.Owner;
import org.slf4j.Logger;
import com.tecsup.petclinic.exception.OwnerNotFoundException;
import com.tecsup.petclinic.repositories.OwnerRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class OwnerServiceImpl implements OwnerService {

    private static final Logger logger = LoggerFactory.getLogger(OwnerServiceImpl.class);

    @Autowired
    OwnerRepository ownerRepository;

    @Override
    public Owner create(Owner owner) {
        return ownerRepository.save(owner);
    }

    @Override
    public Owner update(Owner owner) {
        return ownerRepository.save(owner);
    }

    @Override
    public void delete(Long id) throws OwnerNotFoundException {
        Owner owner = findById(id);
        ownerRepository.delete(owner);

    }

    @Override
    public Owner findById(long id) throws OwnerNotFoundException {
        Optional<Owner> owner = ownerRepository.findById(id);

        if ( !owner.isPresent())
            throw new OwnerNotFoundException("Record not found...!");

        return owner.get();
    }

    @Override
    public List<Owner> findByName(String name) {
        List<Owner> owners = ownerRepository.findByFirstName(name);

        owners.stream().forEach(owner -> logger.info("" + owner));

        return owners;
    }

    @Override
    public List<Owner> findByLastName(String lastname) {
        List<Owner> owners = ownerRepository.findByLastName(lastname);

        owners.stream().forEach(owner -> logger.info("" + owner));

        return owners;
    }

    @Override
    public List<Owner> findByCity(String city) {
        List<Owner> owners = ownerRepository.findByCity(city);

        owners.stream().forEach(owner -> logger.info("" + owner));

        return owners;
    }

    @Override
    public Iterable<Owner> findAll() {
        return ownerRepository.findAll();
    }
}
