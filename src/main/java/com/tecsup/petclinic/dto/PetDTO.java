package com.tecsup.petclinic.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

public class PetDTO {
    @Getter @Setter
    private long id;
    @Getter @Setter
    private String name;
    @Getter @Setter
    private int typeId;
    @Getter @Setter
    private int ownerId;

    //@DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",  timezone = "GMT+8")
    @Getter @Setter
    private Date birthDate;

    public PetDTO(String name, int typeId, int ownerId, Date birthDate) {
        super();
        this.name = name;
        this.typeId = typeId;
        this.ownerId = ownerId;
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Pet [id=" + id + ", name=" + name + ", typeId=" + typeId + ", ownerId=" + ownerId + ", birth_date="
                + birthDate + "]";
    }

}