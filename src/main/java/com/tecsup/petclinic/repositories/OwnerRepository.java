package com.tecsup.petclinic.repositories;

import com.tecsup.petclinic.entities.Owner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OwnerRepository extends CrudRepository<Owner,Long> {
    // Fetch owner by name
    List<Owner> findByFirstName(String name);

    // Fetch owner by lastname
    List<Owner> findByLastName(String lastname);

    // Fetch owner by telephone
    List<Owner> findByCity(String city);


}
